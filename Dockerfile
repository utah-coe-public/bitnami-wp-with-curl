FROM bitnami/wordpress

## Change user to perform privileged actions
USER 0
## Install 'curl'
RUN install_packages curl
## Revert to the original non-root user
USER 1001
